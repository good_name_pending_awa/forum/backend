const Group = require('../models/Group');

const getGroups = async (req, res) => {
  const groups = await Group.find().populate('user', 'name');
  res.json({
    ok: true,
    groups,
  });
};

const createGroup = async (req, res) => {
  const group = new Group(req.body);

  try {
    group.user = req.uid;

    const groupDB = await group.save();

    res.json({
      ok: true,
      group: groupDB,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: 'Hable con el administrador',
    });
  }
};

module.exports = { getGroups, createGroup };
